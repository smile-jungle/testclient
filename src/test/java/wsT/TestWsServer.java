package wsT;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import me.nlp.dream.websocket.WsServer;

/**
 * @author 梦某人
 * @date 2021/9/15 9:21
 */
public class TestWsServer {

    public static void main(String[] args) throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("status",200);
        JSONArray jsonArray  = new JSONArray();
        jsonArray.add("123467832123412412412");
        jsonObject.put("device_id",jsonArray);
        JSONArray jsonArray1 = new JSONArray();
        jsonArray1.add("test");
        jsonObject.put("info_type",1);
        jsonObject.put("data",jsonArray1);
        jsonObject.put("factory_id","001");
        System.out.println(jsonObject);
        for(int i= 0;i<1000;i++){
            int x = (int) (Math.random()*4);
            System.out.println(x);
        }
    }
}
