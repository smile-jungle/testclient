package testAll;

import me.nlp.dream.websocket.WsServer;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * @author 梦某人
 * @date 2021/9/15 10:06
 */
public class tcpAndWs {

    public static final String SERVICE_IP = "0.0.0.0";

    public static final int SERVICE_PORT = 10101;

    public static final char END_CHAR = '#';

    public static void main(String[] args) throws UnknownHostException {

        tcpAndWs t = new tcpAndWs();
        t.startService(SERVICE_IP, SERVICE_PORT);
    }

    private void startService(String serverIP, int serverPort) throws UnknownHostException {
        try {
            //封装服务端地址
            InetAddress serverAddress = InetAddress.getByName(serverIP);
            WsServer w = new WsServer(8887);
            w.start();
            System.out.println("WsServer started on port: " + w.getPort());
            System.out.println("TCPServer will start on port: " + serverPort);
            //建立服务端
            try (ServerSocket service = new ServerSocket(serverPort, 10, serverAddress)) {
                while (true) {
                    StringBuilder receiveMsg = new StringBuilder();
                    //接受一个连接，该方法会阻塞程序，直到一个链接到来
                    try (Socket connect = service.accept()) {
                        //获得输入流
                        InputStream in = connect.getInputStream();

                        //解析输入流，遇到终止符结束，该输入流来自客户端
                        for (int c = in.read(); ; c = in.read()) {
                            if (c == -1)
                                break;
                            receiveMsg.append((char) c);
                        }

                        //组建响应信息
                        String response = "Hello,TCPClient,Your message is:" + receiveMsg.toString();
                        String response2 = "Hello,WsClient,Your message is:" + receiveMsg.toString();
                        w.broadcast(response2);
                        //获取输入流，并通过向输出流写数据的方式发送响应
                        OutputStream out = connect.getOutputStream();
                        out.write(response.getBytes());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }finally {
            //TODO
        }
    }

}
