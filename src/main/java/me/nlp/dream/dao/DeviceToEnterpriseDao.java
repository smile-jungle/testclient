package me.nlp.dream.dao;

/**
 * Created by yang on 2022/2/10 10:57
 */
import com.icecreamqaq.yudb.jpa.JPADao;
import me.nlp.dream.entity.DeviceToEnterprise;

/**
 * @author 梦某人
 * @date 2021/12/8 16:15
 */
public interface DeviceToEnterpriseDao extends JPADao<DeviceToEnterprise,String> {

    DeviceToEnterprise findByIccId(String iccid);

}
