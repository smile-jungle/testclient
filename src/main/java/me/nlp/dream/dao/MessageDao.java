package me.nlp.dream.dao;

/**
 * Created by yang on 2022/2/10 15:15
 */

import com.icecreamqaq.yudb.jpa.JPADao;
import me.nlp.dream.entity.Message;

import java.util.List;

/**
 * @author 梦某人
 * @date 2021/10/7 9:24
 */
public interface MessageDao  extends JPADao<Message,Integer> {

    /**
     *
     * @param deviceId
     * @return
     */
    List<Message> findByDeviceid(String deviceId);
}

