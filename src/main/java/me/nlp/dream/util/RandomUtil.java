package me.nlp.dream.util;

import lombok.NonNull;

import java.util.Random;

/**
 * Created by yang on 2022/2/17 21:23
 */
public class RandomUtil {
    public static Integer generateRandomInteger(int min,int max) throws Exception {
        if(min>max)
        {
            throw new Exception("min < max");
        }
        else if(min==max)
        {
            return Integer.valueOf(min);
        }
        else{
            Random random=new Random();
            int num=min + random.nextInt(max);
            return Integer.valueOf(num);
        }
    }
    public static Integer generateRandomInteger() throws Exception {
        int max=40;
//        int min=-10000;
        int min=30;
        return generateRandomInteger(min,max);
    }

    /**
     * 之所以这么麻烦是想控制生成数的length，因而既要控制范围，也要控制小数精度
     * 所以这里Float型数据分两次生成整数部分和小数部分，主要是由于数据库表里面将Float型数据length设定为了12
     * @return
     * @throws Exception
     */
    public static Float generateRandomFloat() throws Exception {

        int beforeDot=generateRandomInteger();
        int afterDot=generateRandomInteger();

//        System.out.println("整数部分："+beforeDot);
//        System.out.println("小数部分："+afterDot);
        float xiaoshu=(float) afterDot/100;
        float num=beforeDot+xiaoshu;

        return Float.valueOf(num);

    }
    public static String generateRandomString()
    {
        return "wuwu";
    }

}
