package me.nlp.dream.util;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.util.CharsetUtil;

import java.math.BigInteger;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 梦某人
 * @date 2021/9/25 16:00
 */
public class Utils {

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    /***
     * 将Message打包成JSON格式,添加必要信息
     * @param deviceId 设备id
     * @param message 信息主体
     * @return
     */
    public static JSONObject messageToJSON(String deviceId,String message){
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("deviceId",deviceId);
        jsonObject.put("time",System.currentTimeMillis());
        jsonObject.put("status_code",200);
        jsonObject.put("message",message);

        return jsonObject;
    }

    /**
     * 将字符串转为md5
     * @param plainText
     * @return
     */
    public static String stringToMd5(String plainText) {
        byte[] secretBytes = null;
        try {
            secretBytes = MessageDigest.getInstance("md5").digest(
                    plainText.getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("没有这个md5算法！");
        }
        StringBuilder md5code = new StringBuilder(new BigInteger(1, secretBytes).toString(16));
        for (int i = 0; i < 32 - md5code.length(); i++) {
            md5code.insert(0, "0");
        }
        return md5code.toString();
    }

    /***
     * 转换成为信息主题
     * @param password 校验头
     * @param instruction 指令
     * @param endChar 结束符
     * @return
     */
   public static ByteBuf sendinstruction(String password,String instruction,String endChar ){
        //TODO
       return Unpooled.copiedBuffer(password+instruction+endChar,CharsetUtil.UTF_8);
   }
}
