package me.nlp.dream.tcp;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 梦某人
 * @date 2021/9/13 16:06
 */
public class TcpServer {
    public static final String SERVICE_IP = "127.0.0.1";

    public static final int SERVICE_PORT = 8088;

    public static final char END_CHAR = '#';

    SimpleDateFormat format = new SimpleDateFormat("hh-MM-ss");

    public static void main(String[] args) {
        TcpServer service1 = new TcpServer();
        service1.startService();
    }

    private void startService(){
        try {
            InetAddress address = InetAddress.getByName(SERVICE_IP);
            Socket connect = null;
            ExecutorService pool = Executors.newFixedThreadPool(5);
            try (ServerSocket service = new ServerSocket(SERVICE_PORT,5,address)){
                while(true){
                    connect = service.accept();
                    //创建一个任务
                    ServiceTask serviceTask = new ServiceTask(connect);
                    //放入线程池等待运行
                    pool.execute(serviceTask);
                }
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                if(connect!=null)
                    connect.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class ServiceTask implements Runnable{
        private Socket socket;

        ServiceTask(Socket socket){
            this.socket = socket;
        }
        @Override
        public void run() {
            try {
                StringBuilder receiveMsg = new StringBuilder();
                InputStream in = socket.getInputStream();
                for (int c = in.read(); c != END_CHAR; c = in.read()) {
                    if(c ==-1)
                        break;
                    receiveMsg.append((char)c);
                }
                String response = "Hello world " + receiveMsg.toString() + END_CHAR;
                System.out.println("receiveTime:"+ format.format(new Date())+"\nready to response:"+response);
                Thread.sleep(5000);
                System.out.println("receiveTime:"+ format.format(new Date())+"\nsend response:"+response);
                OutputStream out = socket.getOutputStream();
                out.write(response.getBytes());
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                if(socket!=null)
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
            }
        }
    }
}

