package me.nlp.dream.tcp;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * @author 梦某人
 * @date 2021/9/13 16:08
 */
public class TcpClient {

    public static void main(String[] args) {
        TcpClient client = new TcpClient();
        SimpleDateFormat format = new SimpleDateFormat("hh-MM-ss");
        Scanner scanner = new Scanner(System.in);
        while(true){
            String msg = scanner.nextLine();
            if("#".equals(msg))
                break;
            //打印响应的数据
            System.out.println("send time : " + format.format(new Date()));
//            System.out.println(client.sendAndReceive("127.0.0.1",TcpServer.SERVICE_PORT,msg));
            System.out.println(client.sendAndReceive("127.0.0.1",8088,msg));
            System.out.println("receive time : " + format.format(new Date()));
        }
    }

    private String sendAndReceive(String ip, int port, String msg){
        //这里比较重要，需要给请求信息添加终止符，否则服务端会在解析数据时，一直等待
//        msg = msg+TcpServer.END_CHAR;
        msg = msg+"\r\n";
        StringBuilder receiveMsg = new StringBuilder();
        //开启一个链接，需要指定地址和端口
        try (Socket client = new Socket(ip, port)){
            //向输出流中写入数据，传向服务端
            OutputStream out = client.getOutputStream();
            out.write(msg.getBytes());
//            client.shutdownOutput();
            //从输入流中解析数据，输入流来自服务端的响应
            InputStream in = client.getInputStream();
            for (int c = in.read(); c != '\n'; c = in.read()) {
                if(c==-1)
                    break;
                receiveMsg.append((char)c);
            }
            System.out.println(receiveMsg);
        }catch (Exception e){
            e.printStackTrace();
        }
        return receiveMsg.toString();
    }
}