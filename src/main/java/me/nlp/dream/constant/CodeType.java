package me.nlp.dream.constant;

/**
 *    Created by yang on 2022/3/26 10:22
 *    code: 模仿http状态码
 *    description:返回给websocket客户端的描述信息
 */
public enum CodeType {

    SUCCESS_STATUS(0, "成功"),

    //设备连接相关,成功或失败都需向平台客户端发送信息，使其改变设备的在线状态
    DEVICE_CONNECTED(100,"设备已建立连接"),
    DEVICE_DISCONNECTED(101,"设备正常关闭连接"),
    DEVICE_DISCONNECTED_EXCEPTION(103,"设备连接出现异常，已断开连接"),
    UNKNOWN_DEVICE_REFUSED(104,"未注册的设备企图建立连接，已拒绝"),

    //设备经服务端给websocket客户端的信息
    DEVICE_MESSAGE_NORMAL(200,"设备发给平台客户端正常信息"),
    DEVICE_DATA_FORMAT_ERROR(204,"设备发送给数据库的数据格式出错"),

    //websocket客户端向服务端请求，服务端向客户端返回的信息
    REQUEST_ALL_INFO_SUCCESS(300,"请求所有注册设备信息成功"),
    SEND_MESSAGE_TO_ALL_DEVICE_SUCCESS(301,"平台客户端向所有设备发送信息发送成功"),
    SEND_MESSAGE_TO_ALL_DEVICE_FAIL(302,"平台客户端向部分设备发送信息失败，可能是因为这些设备尚未连接"),
    REQUEST_JSON_FORMAT_ERROR(303,"平台客户端向服务端发送的json格式有误"),
    REQUEST_UNKNOWN_INFO_TYPE(304,"平台客户端向服务端发送的info_type不属于服务端定义的内容"),
    REQUEST_ALL_ALIVE_DEVICES(305,"请求企业的所有在线设备信息成功"),

    //websocket客户端建立连接相关
    WsCLIENT_CONNECTED(400,"平台客户端与服务器已建立连接"),
    WsCLIENT_DISCONNECTED_EXCEPTION(401,"平台客户端与服务器的连接出现异常，已断开连接"),
    WsCLIENT_DISCONNECTED(404,"平台客户端与服务器连接已关闭"),


    ;

    private int code;

    private String description;

    CodeType(int code, String description) {
        this.code = code;
        this.description = description;
    }

    public int getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }


}
//   WebSocket建立连接时其实有很多情况，比如
//   出现异常断开连接；发送了错误格式的json数据；提交的info_type识别等等。
//   但不是每个情况都需要websocket客户端进行相应处理，大部分只需要打印出状态码status，和错误描述信息description即可
//
//   需要websocket客户端在onMessage方法中处理的就是本目录下的所有接口，因为有些涉及到与设备的交互，需要获取设备发来的信息；
//   还有就是需要对指定的ICCID的设备发送信息，但有些设备没有连接，需要需要websocket获取这些发送信息失败的设备ICCID并在前端把这些设备icon显示为离线状态。
//
//   全部异常及请求成功情况状态码和描述信息如下：
//   其中数字是所有接口中返回的status,字符串是description
//       SUCCESS_STATUS(0, "成功"),
//
//       //设备连接相关,成功或失败都需向平台客户端发送信息，使其改变设备的在线状态
//       DEVICE_CONNECTED(100,"设备已建立连接"),
//       DEVICE_DISCONNECTED(101,"设备正常关闭连接"),
//       DEVICE_DISCONNECTED_EXCEPTION(103,"设备连接出现异常，已断开连接"),
//       UNKNOWN_DEVICE_REFUSED(104,"未注册的设备企图建立连接，已拒绝"),
//
//       //设备经服务端给websocket客户端的信息
//       DEVICE_MESSAGE_NORMAL(200,"设备发给平台客户端正常信息"),
//       DEVICE_DATA_FORMAT_ERROR(204,"设备发送给数据库的数据格式出错"),
//
//       //websocket客户端向服务端请求，服务端向客户端返回的信息
//       REQUEST_ALL_INFO_SUCCESS(300,"请求所有设备信息成功"),
//       SEND_MESSAGE_TO_ALL_DEVICE_SUCCESS(301,"平台客户端向所有设备发送信息发送成功"),
//       SEND_MESSAGE_TO_ALL_DEVICE_FAIL(302,"平台客户端向部分设备发送信息失败，可能是因为这些设备尚未连接"),
//       REQUEST_JSON_FORMAT_ERROR(303,"平台客户端向服务端发送的json格式有误"),
//       REQUEST_UNKNOWN_INFO_TYPE(304,"平台客户端向服务端发送的info_type不属于服务端定义的内容"),
//       REQUEST_ALL_ALIVE_DEVICES(305,"请求企业的所有在线设备信息成功"),
//
//       //websocket客户端建立连接相关
//       WsCLIENT_CONNECTED(400,"平台客户端与服务器已建立连接"),
//       WsCLIENT_DISCONNECTED_EXCEPTION(401,"平台客户端与服务器的连接出现异常，已断开连接"),
//       WsCLIENT_DISCONNECTED(404,"平台客户端与服务器连接已关闭"),

