package me.nlp.dream.constant;

/**
 * Created by yang on 2022/3/26 18:46
 * websocket客户端传给服务器的json中的info_type
 */
public class InfoType {
    public static final int MESSAGE=1;
    public static final int INSTRUCTIONS=2;
    public static final int ALIVE_DEVICES=3;
}
