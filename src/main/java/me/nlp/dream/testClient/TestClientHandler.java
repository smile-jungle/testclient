package me.nlp.dream.testClient;

import com.alibaba.fastjson.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.CharsetUtil;
import me.nlp.dream.constant.Constants;
import me.nlp.dream.util.Utils;

import javax.swing.JTextArea;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author 梦某人
 * @date 2021/10/8 22:09
 */

public class TestClientHandler extends SimpleChannelInboundHandler<String> {

    private JTextArea infoWindow;
    String password;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public TestClientHandler(JTextArea infoWindow){
        this.infoWindow = infoWindow;
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        super.channelInactive(ctx);
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        super.channelActive(ctx);
        infoWindow.append("server: client connected!"+"\n\n");

    }





    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        infoWindow.append("server: "+msg+"\n\n");
        String orderHead="AT+";
        String queryHead="Query";           //在服务端TcpHandler的channelRead0方法中定义
        String setpassdHead="SETPASSWD=";
        String locationCode="650102";
        String responseHead="$toServer$:";  //在服务端TcpHandler的checkMessageFormat方法中定义
//        msg=msg.substring(0,msg.length()-2);    //去掉END_CHAR: \r\n
//        infoWindow.append("msg: "+msg+"\n\n");

        if(msg.startsWith(orderHead))
        {
            int index=msg.indexOf("$PASSWD$");  ////在服务端WsServer的onMessage方法中定义
            System.out.println("index:"+index);
            String passwd=msg.substring(index+8);
            infoWindow.append("从指令中获取的passwd="+passwd+"\n\n");
            if(passwd.equals(password))
            {
                //执行
                String order=msg.substring(0,index);
                if(order.equals("AT+GETLOCATION"))
                {
                    String res="当前设备所在地点：北京中关村";
                    ctx.writeAndFlush(responseHead+res+Constants.ENDCHAR);
                    infoWindow.append("Client: 当前指令执行结果：\n"+res+"\n\n");
                }
                else {
                    String res="未识别的指令，当前设备仅支持识别AT+GETLOCATION指令以获取设备所在地点";
                    ctx.writeAndFlush(responseHead+res+Constants.ENDCHAR);
                    infoWindow.append("Client: 当前指令执行结果：\n"+res+"\n\n");
                }

            }
            else {
                infoWindow.append("密码不正确，拒绝执行来自server的 "+msg+"指令\n\n");
                ctx.writeAndFlush(responseHead+"由于附加密码不正确，设备拒绝执行该指令"+Constants.ENDCHAR);
                return;
            }
            //返回经纬度
        }
        else if(msg.startsWith(setpassdHead))
        {
            password=msg.substring(10);
            infoWindow.append("当前保存的passwd="+password+"\n\n");
        }
        else if(msg.startsWith(queryHead))
        {
            //注册信息
            String splitStr=";";
            String responseInfo=
                    queryHead+splitStr+
                    Constants.enterprise_id+splitStr+
                            Constants.smallclass+splitStr+
                            Constants.ICCID+splitStr+locationCode;
            ctx.writeAndFlush(responseInfo+Constants.ENDCHAR);
            infoWindow.append("client: "+responseInfo+"\n\n");
        }
        else{

        }
    }

//    @Override
//    public void channelReadComplete(ChannelHandlerContext ctx) throws Exception {
//        System.out.println(ctx.channel().remoteAddress());
//        System.out.println("client received:");
//        //ctx.writeAndFlush("12345678901234567890");
//    }
}
