package me.nlp.dream.testClient;

/**
 * Created by yang on 2022/3/16 15:08
 */

import com.alibaba.fastjson.JSONObject;

import java.io.BufferedInputStream;
import java.net.Socket;

public class Client {
    public Client() throws Exception {
        Socket socket = new Socket("localhost", 10101);

        BufferedInputStream bis = new BufferedInputStream(socket.getInputStream());

        byte[] buf = new byte[256];

        System.out.println("开始读数据...");
        int count = bis.read(buf);
        System.out.println("读取数据数量:" + count);
        System.out.println(new String(buf));

        bis.close();
        socket.close();
    }

    public static void main(String[] args) {

        String jsonStr="{\"enterprise_id\": \"1101010001\",\"devices\": [\"12345678901234567801\",\"12345678901234567802\",\"12345678901234567803\"],\"message\": \"AT+hello\",\"info_type\": 1}";
//        System.out.println(jsonStr);
        String str="{\"info_type\":1,\"devices\":[\"12345678901234567801\",\"12345678901234567802\",\"12345678901234567803\"],\"enterprise_id\":\"1101010001\",\"message\":\"AT+hello\"}";
        JSONObject jsonObject = JSONObject.parseObject(jsonStr);
        System.out.println("\n\njsonObject:"+jsonObject.toJSONString());

//        try {
//            new Client();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
}
