package me.nlp.dream.testClient;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.SneakyThrows;
import me.nlp.dream.constant.Constants;
import me.nlp.dream.entity.DeviceToEnterprise;
import me.nlp.dream.pojo.Attribute;
import me.nlp.dream.service.AllSqlService;
import me.nlp.dream.service.DeviceToEnterpriseService;
import me.nlp.dream.util.RandomUtil;

import javax.inject.Inject;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;
import java.util.Timer;


/**
 * Created by yang on 2022/1/17 20:46
 */

public class TestClient2 extends JFrame {

    boolean isConnected;

    EventLoopGroup eventLoopGroup;
    ChannelFuture channelFuture;

    NativeSocketClient nativeSocketClient;

    DeviceToEnterpriseService deviceToEnterpriseService;

    AllSqlService allSqlService;

    List<Attribute> attributeList;

    public static int width=1000;
    public static int half_width=500;
    public static int height=600;

    //发送的数据末尾必须是END_CHAR，否则服务端接收不到信息(缓存未满？)，这可由开发者控制
    public static String END_CHAR="\r\n";

    //默认加载的配置
    public  String my_ip="127.0.0.1";
//    public  String my_ip="211.81.248.108";
    public  int my_port=8088;
    public  String my_heartbeat="20220401092022041109";
//    public  String my_heartbeat="12345678901234567801";
    //20220429195412000002

    public  String my_sendMsg="hello, I am device Client";

    //最终发送时的配置
    String final_ip;
    int final_port;
    String final_heartbeat;
    String final_sendMsg;



    //需要从信息框里获取信息的组件才作为属性，因为需要在其它方法中调用并获取信息，而像JButton,JLabel在方法体里new即可
    private JTextField address;
    private JTextField port;
    private JTextField registPackage;

    private JTextArea infoWindow;
    private JTextArea sendMegWindow;

    //自动发送数据定时器开关
    private JButton timeGapBtn;

    //发送数据间隔定时器配置
    boolean isTimerOn;
    int curTimerGap;
    Timer sendMsgTimer;

    public TestClient2()
    {
        super("DeviceClient");

        initGUI();
        initParameters();
    }
    public TestClient2(String iccid)
    {
        super("DeviceClient");

        initGUI();
        initParameters();
        setFinal_heartbeat(iccid);
    }
    void setFinal_heartbeat(String iccid)
    {
//        my_heartbeat=iccid;
    }
    public void setDeviceToEnterpriseService(DeviceToEnterpriseService deviceToEnterpriseService)
    {
        this.deviceToEnterpriseService=deviceToEnterpriseService;
    }
    public void setAllSqlService(AllSqlService allSqlService)
    {
        this.allSqlService=allSqlService;
    }
    public void initGUI(){
        setSize(width,height);
        Container c=getContentPane();
        c.setLayout(new BorderLayout());

        JPanel panelLeft=new JPanel();
        JPanel panelRight=new JPanel();

        //左边和右边面板容器（相当于div）
        CostumeLeftPanel(panelLeft);
        CostumeRightPanel(panelRight);

        c.add(panelLeft,BorderLayout.WEST);
        c.add(panelRight,BorderLayout.EAST);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);


    }
    void initParameters()
    {
        final_ip="";
        final_port=0;
        final_heartbeat="";
        final_sendMsg="";

        isConnected=false;
        isTimerOn=false;
        curTimerGap=2;
    }
    public void CostumeLeftPanel(JPanel jp)
    {
//        jp.setBackground(Color.BLACK);
        jp.setPreferredSize(new Dimension(half_width,height));
        jp.setLayout(new FlowLayout(FlowLayout.LEFT));

//        BoxLayout boxLayout=new BoxLayout(jp,BoxLayout.Y_AXIS);
//        jp.setLayout(boxLayout);
        //
        JPanel jp1=new JPanel();
        jp1.setPreferredSize(new Dimension(half_width,100));
        jp1.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel label1=new JLabel("ip地址:");
        JLabel label2=new JLabel("端口:");

        address=new JTextField(20);
        port=new JTextField(10);

        jp1.add(label1);
        jp1.add(address);
        jp1.add(label2);
        jp1.add(port);

        //
        JPanel jp2=new JPanel();
        jp2.setPreferredSize(new Dimension(half_width,100));
        jp2.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel label3=new JLabel("网络包:");
        registPackage=new JTextField(20);

        jp2.add(label3);
        jp2.add(registPackage);

        //
        JPanel jp3=new JPanel();
        jp3.setPreferredSize(new Dimension(half_width,100));
        jp3.setLayout(new FlowLayout(FlowLayout.CENTER));

        JButton load=new JButton("load");
        JButton run=new JButton("run");
        JButton close=new JButton("close");

        //test load btn
//        System.out.println("pannel:"+load.getText());

        load.addActionListener(new LoadPropertitiesListener());
        run.addActionListener(new RunNettyClientListener());
        close.addActionListener(new CloseNettyClientListener());

        jp3.add(load);
        jp3.add(run);
        jp3.add(close);

        //跟网页布局的flow:left一样
        JPanel jp4=new JPanel();
        jp4.setPreferredSize(new Dimension(half_width,100));
        FlowLayout flowLayout4=new FlowLayout(FlowLayout.CENTER);
        jp4.setLayout(flowLayout4);
        flowLayout4.setHgap(20);

        JLabel timeGapLabel=new JLabel("请选择定时发送时间间隔，单位:s");
        JComboBox timeGap=new JComboBox();
        timeGap.addItem("2");
        timeGap.addItem("4");
        timeGap.addItem("6");
        timeGap.addItem("8");
        timeGap.addItem("120");
        timeGapBtn=new JButton("开启");   //还有关闭，共用一个按钮

        timeGapBtn.addActionListener(new StartTimerListener());
        timeGap.addActionListener(new TimerJComboxListener());

        jp4.add(timeGapLabel);
        jp4.add(timeGap);
        jp4.add(timeGapBtn);


        //把JPanel当成div
        jp.add(jp1);
        jp.add(jp2);
        jp.add(jp3);
        jp.add(jp4);

//        jp.add(new JButton("thirdButton"));
//        jp.add(btn1);

    }
    public void CostumeRightPanel(JPanel jp){

        jp.setPreferredSize(new Dimension(half_width,height));
        jp.setLayout(new BorderLayout());

        JPanel jp1=new JPanel();
        jp1.setPreferredSize(new Dimension(half_width,300));
//        jp1.setBackground(Color.BLUE);
        jp1.setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel infoLabel=new JLabel("信息窗口");
        infoWindow=new JTextArea(15,40);
        JScrollPane js1=new JScrollPane(infoWindow);

        jp1.add(infoLabel);
        jp1.add(js1);


        JPanel jp2=new JPanel();
        jp2.setPreferredSize(new Dimension(half_width,300));
//        jp2.setBackground(Color.GREEN);
        jp2.setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel sendWindow=new JLabel("发送窗口");
        sendMegWindow=new JTextArea(10,40);
        JScrollPane js2=new JScrollPane(sendMegWindow);

        JButton loadBtn=new JButton("loadMsg");
        JButton sendBtn=new JButton("sendMsg");
        loadBtn.addActionListener(new LoadMsgListener());
        sendBtn.addActionListener(new SendMsgListener());

        jp2.add(sendWindow);
        jp2.add(js2);
        jp2.add(loadBtn);
        jp2.add(sendBtn);


        jp.add(jp1,BorderLayout.NORTH);
        jp.add(jp2,BorderLayout.CENTER);



    }

    /**
     * 向信息窗口追加信息，如日志，状态等
     * @param info
     */
    void appendInfoWindow(String info)
    {
        infoWindow.append(info+"\n\n");
        infoWindow.setCaretPosition(infoWindow.getText().length());
    }

    /**
     * 给设备数据属性样式格式化，如
     * Speed(速度)：Float
     * @param attribute
     * @return
     */
    String AttributeInfoWrapper(Attribute attribute)
    {
        String CnName=attribute.getAttributeName();
        String EnName=attribute.getAttributeRemark();
        String dataType=attribute.getAttributeType();

        String attrInfo=EnName+"("+CnName+"):"+dataType+"  ||  ";
        return attrInfo;
    }
    void showDeviceDataFormat()
    {
        String iccid=final_heartbeat;
        attributeList=fetchDeviceAttributeInfo(iccid);
        if(attributeList==null)
        {
            appendInfoWindow("WARN: 服务器device_enterprise表中没有该设备的注册信息");
            return;
        }

        if(attributeList.size()==0)
        {
            //向信息窗口写入未获取到该设备的数据格式信息
            appendInfoWindow("WARN: 服务器数据库中不存在当前设备的数据属性信息attribute表，可能还没有建立");
            return;
        }

        //当前设备的发送数据属性 英文+中文
        appendInfoWindow("当前设备的数据属性如下：");

        String deviceAttributes="  ||  ";
        for (Attribute attribute : attributeList){
            deviceAttributes=deviceAttributes+AttributeInfoWrapper(attribute);
        }
        appendInfoWindow(deviceAttributes);

    }
    void stopTimer()
    {
        //为什么额外提取出来？用户在关闭定时器之前可能直接关闭socket连接，因而释放资源要单独提取出来
        //而开启定时器不会随着开启连接而开启，且已经在开启定时器代码部分做了判断客户端是否连接
        if(isTimerOn)
        {
            isTimerOn=false;
            timeGapBtn.setText("开启");
            timeGapBtn.updateUI();
            sendMsgTimer.cancel();
            sendMsgTimer=null;
        }
    }
    void loadMsg() throws Exception {
        if(attributeList==null)
        {
            //新设备向device表注册后数据库表更新了，此时再获取数据格式应该会有
            showDeviceDataFormat();
            if(attributeList==null)
            {
                appendInfoWindow("WARN: 服务器device_enterprise表中没有该设备的注册信息");
                return;
            }

        }

        if(attributeList.size()==0)
        {
            //向信息窗口写入未获取到该设备的数据格式信息
            appendInfoWindow("WARN: 服务器数据库中不存在当前设备的数据属性信息attribute表，可能还没有建立");
            return;
        }
        //最好用JSONArray的形式发出去，用JSONObject发出去顺序会变（后者实质上是map）
        JSONArray msg=new JSONArray();

        for(Attribute attribute:attributeList){
            JSONObject jsonObject=new JSONObject();
            String key=attribute.getAttributeName();
            String type=attribute.getAttributeType();
            if(type.equals("float") || type.equals("Float") || type.equals("FLOAT"))
            {
                Float value= RandomUtil.generateRandomFloat();
                jsonObject.put(key,value);
            }
            else if(type.equals("int") || type.equals("Int") || type.equals("INT"))
            {
                Integer value=RandomUtil.generateRandomInteger();
                jsonObject.put(key,value);
            }
            else    //其它类型暂时按String保存
            {
                String value=RandomUtil.generateRandomString();
                jsonObject.put(key,value);
            }
            msg.add(jsonObject);
        }
        final_sendMsg=msg.toString();
        sendMegWindow.setText(final_sendMsg);
    }
    void sendMsg()
    {
        if(isConnected==false){
            appendInfoWindow("ERROR:客户端尚未连接！");
            return;
        }

        String sendMsg=sendMegWindow.getText();
        if(sendMsg==null || sendMsg.equals(""))
        {
            appendInfoWindow("WARN:发送窗口不得为空！");
            return;
        }
        final_sendMsg=sendMsg;
        channelFuture.channel().writeAndFlush(final_sendMsg+END_CHAR);
//        nativeSocketClient.sendMessage(final_sendMsg);
        sendMegWindow.setText("");
        appendInfoWindow("Client: "+final_sendMsg);
    }

    /**
     * 根据iccid获取设备的属性，如speed,weight,height，也即该设备要发送的数据格式
     * @param iccid
     */
    List<Attribute> fetchDeviceAttributeInfo(String iccid)
    {
        DeviceToEnterprise deviceToEnterprise=deviceToEnterpriseService.findByIccid(iccid);

        if(deviceToEnterprise==null)
            return null;
        //代码还没找队友更新，导致我这边LearnNetty服务端运行后，数据库表中的deviceName等内容没了
        String deviceName=deviceToEnterprise.getDeviceName();
        appendInfoWindow("设备名: "+deviceName);

        String deviceId=deviceToEnterprise.getDeviceId();
        String enterpriseId=deviceToEnterprise.getEnterpriseId();

        String deviceType=deviceId.substring(0,4);

        String deviceAttributesTable=enterpriseId+"_"+deviceType+"_attribute";

        java.util.List<Attribute> AttributeList = new ArrayList<>();

        List<Object[]> oList = allSqlService.getSelect(deviceAttributesTable);
        if(oList != null){
            for(Object[] o : oList){
                Attribute attribute = new Attribute();

                attribute.setAttributeName((String)o[0]);
                attribute.setAttributeType((String)o[1]);
                attribute.setAttributeLength((String)o[2]);
                attribute.setAttributeDigits((String)o[3]);
                attribute.setAttributeRemark((String)o[4]);
                attribute.setAttributeCode((String)o[5]);

                AttributeList.add(attribute);
            }
        }
        return AttributeList;
    }
    void loadPropertities()
    {
        String ip1=address.getText();
        String port1=port.getText();
        String heartbeat1=registPackage.getText();


        if(ip1==null || ip1.equals("")){
            address.setText(my_ip);
            final_ip=my_ip;
        }
        else {
            final_ip=ip1;
        }
        address.setText(final_ip);



        if(port1==null || port1.equals("")){
            port.setText(String.valueOf(my_port));
            final_port=my_port;
        }
//            else {
//                int port=Integer.parseInt(port1);
//            }

        if(heartbeat1==null || heartbeat1.equals(""))
        {
            final_heartbeat=my_heartbeat;
        }
        else
        {
            final_heartbeat=heartbeat1;
        }
        registPackage.setText(final_heartbeat);
        Constants.ICCID=final_heartbeat;

        appendInfoWindow("设备连接参数信息已加载...");
//            else{
//                final_sendMsg=sendMsg1;
//            }
    }

    class LoadPropertitiesListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
//            JButton jButton=(JButton)e.getSource();
//            System.out.println("listener:"+jButton.getText());

            loadPropertities();
        }
    }
    boolean isPrepared()
    {
        if(Objects.equals(final_ip, "") || final_port==0 || Objects.equals(final_heartbeat, ""))
            return false;
        return true;
    }
    class RunNettyClientListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(isConnected){
                appendInfoWindow("客户端已经连接成功，无需再连接");
                return;
            }

            if(!isPrepared()){
                appendInfoWindow("连接失败，请先填好连接参数");
                return;
            }
            loadPropertities();

            new NettyClientThread().opreateConnection();
            //以下是原生socket试图与netty服务端建立连接
//            try {
//                nativeSocketClient = new NativeSocketClient(final_ip, final_port);
//                nativeSocketClient.init(final_heartbeat);
//            }catch (IOException ex)
//            {
//                appendInfoWindow("客户端连接服务器失败，请检查服务器是否开启");
//                return;
//            }
//            new Thread(nativeSocketClient).start();

            infoWindow.setText("");
            sendMegWindow.setText("");
            appendInfoWindow(final_heartbeat+"设备连接服务器成功！");
            isConnected=true;
            showDeviceDataFormat();


        }
    }
    class CloseNettyClientListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(isConnected==false)
            {
                appendInfoWindow("客户端与服务器的连接已关闭，请勿重复按关闭按钮");
                return;
            }
            eventLoopGroup.shutdownGracefully();
//            nativeSocketClient.shutDownClient();
            stopTimer();
            isConnected=false;
//            appendInfoWindow("客户端与服务器的连接成功关闭!");
        }
    }
    class NettyClientThread{
        void opreateConnection(){
            new Thread(() -> {
                eventLoopGroup = new NioEventLoopGroup();
                try{
                    Bootstrap bootStrap = new Bootstrap();
                    bootStrap.group(eventLoopGroup).channel(NioSocketChannel.class)
                            .handler(new TestClientInitializer(infoWindow));
                    try {
                        channelFuture = bootStrap.connect(final_ip,final_port).sync();
                        isConnected=true;
                        channelFuture.channel().writeAndFlush(final_heartbeat+END_CHAR);
                        channelFuture.channel().closeFuture().sync();
//                        channelFuture.channel().writeAndFlush("my second test of channelFuture\r\n");


                    } catch (InterruptedException exception) {
                        exception.printStackTrace();
                    }
                }finally {
                    eventLoopGroup.shutdownGracefully();
                    appendInfoWindow("客户端与服务器的连接已关闭");
                }
            }).start();
        }

    }
    class NativeSocketClient implements Runnable{
        Socket socket;
        InputStream is;
        OutputStream os;

        NativeSocketClient(String ip,int port) throws IOException {
            socket=new Socket(ip,port);
            is=socket.getInputStream();
            os=socket.getOutputStream();
//            socket.setTcpNoDelay(true);
//            socket.setSendBufferSize(32*1024);

        }
        @SneakyThrows
        @Override
        public void run() {

            StringBuilder receiveMsg = new StringBuilder();
            char last='a';
            try {
                for (int c = is.read(); c != -1; c = is.read()) {
                    receiveMsg.append((char) c);
                    if (c == '\n') {
                        if (last == '\r') {
                            receiveMessage(receiveMsg.toString());
                            receiveMsg.delete(0, receiveMsg.length());
                        }
                    }
                    last = (char) c;
                }
            }catch (SocketException exception)
            {

            }

        }
        void init(String initMessage) throws IOException {
            this.sendMessage(initMessage);
        }
        void sendMessage(String sendMsg) {
            try{
                sendMsg=sendMsg+END_CHAR;
//                byte[] msg=sendMsg.getBytes();
                os.write(sendMsg.getBytes(StandardCharsets.UTF_8));
//                os.write(msg);
//                os.write(msg,0, msg.length);
                os.flush();
//                ByteBuffer src=new ByteBuffer(sendMsg.getBytes(StandardCharsets.UTF_8));
//                socket.getChannel().write(src);
            }catch (IOException exception)
            {
                exception.printStackTrace();
                appendInfoWindow("出现异常：发送信息失败");
            }

        }
        void receiveMessage(String receiveMsg)
        {
            appendInfoWindow("server: "+receiveMsg);
        }
        void shutDownClient()
        {
            if(socket!=null)
                try {
                    socket.shutdownOutput();
                    socket.shutdownInput();
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                    appendInfoWindow("出现异常：客户端连接未正常关闭");
                }
        }
    }
    class LoadMsgListener implements ActionListener{

        @SneakyThrows
        @Override
        public void actionPerformed(ActionEvent e) {
            loadMsg();
        }
    }
    class StartTimerListener implements ActionListener{

        @SneakyThrows
        @Override
        public void actionPerformed(ActionEvent e) {
            JButton timerOnOff=(JButton) e.getSource();
            String status=timerOnOff.getText();

            if(!isConnected){
                appendInfoWindow("ERROR:客户端尚未连接！");
                return;
            }

            if(status.equals("开启"))
            {
                long newTimerGap=curTimerGap;
                sendMsgTimer=new Timer();
                //表明点击前定时器是关闭状态，因而这里需要开启定时器，并将按钮的文字改为“关闭”
                sendMsgTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            //do Something
                            loadMsg();
                            sendMsg();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },2000,newTimerGap* 1000);
                isTimerOn=true;
                timerOnOff.setText("关闭");
                timerOnOff.updateUI();
                appendInfoWindow("发送数据定时器已开启，当前以"+newTimerGap+" s时间间隔发送数据");

            }
            else{
                stopTimer();
                appendInfoWindow("发送数据定时器已关闭");
            }

        }
    }
    class TimerJComboxListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            JComboBox jComboBox=(JComboBox) e.getSource();
            String numStr=(String) jComboBox.getSelectedItem();
            int num=Integer.parseInt(numStr);
            if(isTimerOn)
            {
                appendInfoWindow("当前定时器正在运行，新的定时器时间间隔将在定时器关闭后再次启动才生效");
            }

            if(num!=curTimerGap)
            {
                curTimerGap=num;
            }
        }
    }
    class SendMsgListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            sendMsg();
        }
    }
}

