package me.nlp.dream.testClient;

import me.nlp.dream.annotation.ApiController;
import me.nlp.dream.annotation.ApiParam;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by yang on 2022/3/3 10:16
 */
public class TestSpring {
    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException {
        TestSpring testSpring=new TestSpring();
        String name="yang";
        String password="taobao123";

        HashMap<String,String> paraMap=new HashMap<>();
        paraMap.put("name",name);
        paraMap.put("password",password);

        ArrayList<String> paraNamesList=new ArrayList<>();

        Class<?> clazz=TestSpring.class;
        Method[] methods= clazz.getDeclaredMethods();
        for(Method method:methods)
        {
            if(!method.isAnnotationPresent(ApiController.class))
                continue;
            Annotation[][] annotations=method.getParameterAnnotations();
            if(annotations==null)
                continue;
            for(Annotation[] paras:annotations)
            {
                for(Annotation para:paras){
                    ApiParam apiParam=(ApiParam)para;
                    String paraName=apiParam.name();
                    paraNamesList.add(paraMap.get(paraName));

                }

            }
            int paraCount=paraNamesList.size();
            String strings[]=new String[paraCount];
            for(int i=0;i<paraCount;i++)
            {
                strings[i]=paraNamesList.get(i);
            }
            method.invoke(testSpring,strings);

        }



    }
    @ApiController(name="login")
    void login(@ApiParam(name="name") String name, @ApiParam(name="password") String password)
    {
        System.out.println("exec login");
        System.out.println("name="+name+",passwd="+password);
    }
}
