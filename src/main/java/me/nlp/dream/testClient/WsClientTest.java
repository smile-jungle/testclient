package me.nlp.dream.testClient;

import com.alibaba.fastjson.JSONObject;
import me.nlp.dream.constant.CodeType;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.channels.NotYetConnectedException;
import java.util.Objects;
import java.util.Timer;

/**
 * Created by yang on 2022/3/25 10:48
 */
public class WsClientTest{
    JFrame wsClient;
    MyWebSocketClient myWebSocketClient;

    public static int width=1000;
    public static int half_width=500;
    public static int height=600;

    //发送的数据末尾必须是END_CHAR，否则服务端接收不到信息(缓存未满？)，这可由开发者控制
    public static String END_CHAR="\r\n";

    //默认加载的配置
    //my_ip没用，下面的final_uri才有用
    public static String my_ip="127.0.0.1";
//    public static String my_ip="211.81.248.108";
    public static int my_port=8089;
    public static String my_heartbeat="12345678901234567801";
    public static String my_sendMsg="hello, I am device Client";

    //最终发送时的配置
    String final_ip;
    int final_port;
    String final_heartbeat;
//    String final_sendMsg="{\"info_type\":1,\"devices\":[\"12345678901234567801\"],\"enterprise_id\":\"1101010001\",\"message\":\"AT+hello\"}";
    String final_sendMsg="{\"info_type\":4,\"enterprise_id\":\"1101010001\",\"message\":\"AT+GETLOCATION\"}";
//    String final_sendMsg="{\"info_type\":1,\"devices\":[\"12345678901234567801\",\"12345678901234567802\",\"12345678901234567803\"],\"enterprise_id\":\"1101010001\",\"message\":\"AT+hello\"}";
//    String final_sendMsg="{\"info_type\":3,\"enterprise_id\":\"1101010001\"}";    //请求获取该企业所有在线设备信息
//    String final_sendMsg


//    String final_uri="ws://211.81.248.108:8089/1101010001";
    String final_uri="ws://127.0.0.1:8089/1101010001";


    boolean isConnected;

    //需要从信息框里获取信息的组件才作为属性，因为需要在其它方法中调用并获取信息，而像JButton,JLabel在方法体里new即可
    private JTextField address;
    private JTextField port;
    private JTextField registPackage;

    private JTextArea infoWindow;
    private JTextArea sendMegWindow;

    //自动发送数据定时器开关
    private JButton timeGapBtn;

    //发送数据间隔定时器配置
    boolean isTimerOn;
    int curTimerGap;
    Timer sendMsgTimer;

    public void initGUI(){
        wsClient=new JFrame("WsClient");
        wsClient.setSize(width,height);
        Container c=wsClient.getContentPane();
        c.setLayout(new BorderLayout());

        JPanel panelLeft=new JPanel();
        JPanel panelRight=new JPanel();

        //左边和右边面板容器（相当于div）
        CostumeLeftPanel(panelLeft);
        CostumeRightPanel(panelRight);

        c.add(panelLeft,BorderLayout.WEST);
        c.add(panelRight,BorderLayout.EAST);

        wsClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        wsClient.setVisible(true);


    }
    void initParameters()
    {
        final_ip="";
        final_port=0;
        final_heartbeat="";

        isConnected=false;
        isTimerOn=false;
        curTimerGap=2;
    }
    public void CostumeLeftPanel(JPanel jp)
    {
//        jp.setBackground(Color.BLACK);
        jp.setPreferredSize(new Dimension(half_width,height));
        jp.setLayout(new FlowLayout(FlowLayout.LEFT));

//        BoxLayout boxLayout=new BoxLayout(jp,BoxLayout.Y_AXIS);
//        jp.setLayout(boxLayout);
        //
        JPanel jp1=new JPanel();
        jp1.setPreferredSize(new Dimension(half_width,100));
        jp1.setLayout(new FlowLayout(FlowLayout.LEFT));

//        JLabel label1=new JLabel("ip地址:");
//        JLabel label2=new JLabel("端口:");
//
//        address=new JTextField(20);
//        port=new JTextField(10);
//
//        jp1.add(label1);
//        jp1.add(address);
//        jp1.add(label2);
//        jp1.add(port);
//
//        //
//        JPanel jp2=new JPanel();
//        jp2.setPreferredSize(new Dimension(half_width,100));
//        jp2.setLayout(new FlowLayout(FlowLayout.LEFT));
//
//        JLabel label3=new JLabel("网络包:");
//        registPackage=new JTextField(20);
//
//        jp2.add(label3);
//        jp2.add(registPackage);

        //
        JPanel jp3=new JPanel();
        jp3.setPreferredSize(new Dimension(half_width,100));
        jp3.setLayout(new FlowLayout(FlowLayout.CENTER));

        JButton load=new JButton("load");
        JButton run=new JButton("run");
        JButton close=new JButton("close");

//        load.addActionListener(new TestClient2.LoadPropertitiesListener());
        run.addActionListener(new WsClientTest.RunWsClientListener());
        close.addActionListener(new WsClientTest.CloseWsClientListener());

        jp3.add(load);
        jp3.add(run);
        jp3.add(close);

        //跟网页布局的flow:left一样
        JPanel jp4=new JPanel();
        jp4.setPreferredSize(new Dimension(half_width,100));
        FlowLayout flowLayout4=new FlowLayout(FlowLayout.CENTER);
        jp4.setLayout(flowLayout4);
        flowLayout4.setHgap(20);

        JLabel timeGapLabel=new JLabel("请选择定时发送时间间隔，单位:s");
        JComboBox timeGap=new JComboBox();
        timeGap.addItem("2");
        timeGap.addItem("4");
        timeGap.addItem("6");
        timeGap.addItem("8");
        timeGapBtn=new JButton("开启");   //还有关闭，共用一个按钮

//        timeGapBtn.addActionListener(new TestClient2.StartTimerListener());
//        timeGap.addActionListener(new TestClient2.TimerJComboxListener());

        jp4.add(timeGapLabel);
        jp4.add(timeGap);
        jp4.add(timeGapBtn);


        //把JPanel当成div
        jp.add(jp1);
//        jp.add(jp2);
        jp.add(jp3);
        jp.add(jp4);

//        jp.add(new JButton("thirdButton"));
//        jp.add(btn1);

    }
    public void CostumeRightPanel(JPanel jp){

        jp.setPreferredSize(new Dimension(half_width,height));
        jp.setLayout(new BorderLayout());

        JPanel jp1=new JPanel();
        jp1.setPreferredSize(new Dimension(half_width,300));
//        jp1.setBackground(Color.BLUE);
        jp1.setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel infoLabel=new JLabel("信息窗口");
        infoWindow=new JTextArea(15,40);
        JScrollPane js1=new JScrollPane(infoWindow);

        jp1.add(infoLabel);
        jp1.add(js1);


        JPanel jp2=new JPanel();
        jp2.setPreferredSize(new Dimension(half_width,300));
//        jp2.setBackground(Color.GREEN);
        jp2.setLayout(new FlowLayout(FlowLayout.CENTER));

        JLabel sendWindow=new JLabel("发送窗口");
        sendMegWindow=new JTextArea(10,40);
        JScrollPane js2=new JScrollPane(sendMegWindow);

        JButton loadBtn=new JButton("loadMsg");
        JButton sendBtn=new JButton("sendMsg");
        loadBtn.addActionListener(new LoadSendMsgListener());
        sendBtn.addActionListener(new SendMsgListener());

        jp2.add(sendWindow);
        jp2.add(js2);
        jp2.add(loadBtn);
        jp2.add(sendBtn);


        jp.add(jp1,BorderLayout.NORTH);
        jp.add(jp2,BorderLayout.CENTER);

    }
    public WsClientTest() {
        initGUI();
        initParameters();
    }

    class RunWsClientListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            if(isConnected){
                appendInfoWindow("客户端已经连接成功，无需再连接");
                return;
            }
            try {
                myWebSocketClient = new MyWebSocketClient(new URI(final_uri));
            }catch (URISyntaxException exception)
            {
                appendInfoWindow("创建客户端失败");
                exception.printStackTrace();
            }
            myWebSocketClient.connect();

            infoWindow.setText("");
            sendMegWindow.setText("");
            appendInfoWindow("客户端尝试连接服务器中...");
            isConnected=true;


        }
    }
    class CloseWsClientListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(isConnected==false)
            {
                appendInfoWindow("客户端与服务器的连接已关闭，请勿重复按关闭按钮");
                return;
            }
            stopTimer();
            myWebSocketClient.close();
            myWebSocketClient=null;
            isConnected=false;
            appendInfoWindow("客户端与服务器的连接成功关闭!");
        }
    }
    class LoadSendMsgListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            sendMegWindow.setText(final_sendMsg);
            sendMegWindow.updateUI();
        }
    }
    class SendMsgListener implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            if(isConnected==false){
                appendInfoWindow("ERROR:客户端尚未连接！");
                return;
            }

            String sendMsg=sendMegWindow.getText();
            if(sendMsg==null || sendMsg.equals(""))
            {
                appendInfoWindow("WARN:发送窗口不得为空！");
                return;
            }
            final_sendMsg=sendMsg;
            myWebSocketClient.send(final_sendMsg);
            sendMegWindow.setText("");
            appendInfoWindow("Client: "+final_sendMsg);
        }
    }
    /**
     * 向信息窗口追加信息，如日志，状态等
     * @param info
     */
    void appendInfoWindow(String info)
    {
        infoWindow.append(info+"\n\n");
        infoWindow.setCaretPosition(infoWindow.getText().length());
    }
    void stopTimer()
    {
        //为什么额外提取出来？用户在关闭定时器之前可能直接关闭socket连接，因而释放资源要单独提取出来
        //而开启定时器不会随着开启连接而开启，且已经在开启定时器代码部分做了判断客户端是否连接
        if(isTimerOn)
        {
            isTimerOn=false;
            timeGapBtn.setText("开启");
            timeGapBtn.updateUI();
            sendMsgTimer.cancel();
            sendMsgTimer=null;
        }
    }
    boolean isPrepared()
    {
        if(Objects.equals(final_ip, "") || final_port==0 || Objects.equals(final_heartbeat, ""))
            return false;
        return true;
    }
    class MyWebSocketClient extends WebSocketClient{

        MyWebSocketClient(URI uri) {
            super(uri);
        }

        @Override
        public void onOpen(ServerHandshake serverHandshake) {
            appendInfoWindow("客户端成功连接服务器");
            String request_alive_devices="{\"info_type\":3,\"enterprise_id\":\"1101010001\"}";
            this.send(request_alive_devices);
        }

        @Override
        public void onMessage(String s) {
//            appendInfoWindow("from device:"+s);
            try{
                JSONObject message=JSONObject.parseObject(s);
                int status=message.getInteger("status");
                String description=message.getString("description");
                JSONObject data=message.getJSONObject("data");

                String standardInfo=getCodeAndDescription(status,description);
//                appendInfoWindow(standardInfo);
                if(status== CodeType.REQUEST_ALL_ALIVE_DEVICES.getCode())
                {
                    int alive_count=data.getInteger("alive_count");
                    String moreInfo="具体描述信息:";
                    moreInfo+="当前企业有"+alive_count+"个在线设备";
                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status== CodeType.DEVICE_DATA_FORMAT_ERROR.getCode())
                {
                    String moreInfo="具体描述信息:"+data.getString("message");
                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status==CodeType.DEVICE_MESSAGE_NORMAL.getCode())
                {
                    String moreInfo="具体描述信息:"+data.getString("message");
                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status==CodeType.DEVICE_CONNECTED.getCode())
                {
                    String moreInfo="具体描述信息:";
                    String ICCID=data.getString("ICCID");
                    moreInfo+="设备ICCID为"+ICCID+"已上线";

                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status==CodeType.DEVICE_DISCONNECTED.getCode())
                {
                    String moreInfo="具体描述信息:";
                    String ICCID=data.getString("ICCID");
                    moreInfo+="设备ICCID为"+ICCID+"已正常下线";

                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status==CodeType.DEVICE_DISCONNECTED_EXCEPTION.getCode())
                {
                    String moreInfo="具体描述信息:";
                    String ICCID=data.getString("ICCID");
                    moreInfo+="设备ICCID为"+ICCID+"出现异常下线";

                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                if(status==CodeType.REQUEST_ALL_INFO_SUCCESS.getCode())
                {
                    String moreInfo="具体描述信息:";
                    moreInfo+=message;
                    appendInfoWindow(standardInfo+moreInfo);
                    return;
                }

                appendInfoWindow(standardInfo+standardInfo);





            }catch (RuntimeException e)
            {
                appendInfoWindow("接收到的信息不符合json格式，无法解析");
            }
        }

        @Override
        public void onClose(int i, String s, boolean b) {
            appendInfoWindow("客户端正常关闭");
            if(!this.isClosed())
                this.close();
        }

        @Override
        public void onError(Exception e) {
            appendInfoWindow("客户端异常关闭");
        }

        String getCodeAndDescription(int code,String descrition)
        {
            String standardInfo="客户端接收到新的消息:\n";
            standardInfo+="状态码Code:"+code+"\n简单描述信息:"+descrition+"\n";

            return standardInfo;
        }
    }

    public static void main(String[] args) {
        new WsClientTest();
    }
}
