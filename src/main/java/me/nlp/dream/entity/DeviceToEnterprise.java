package me.nlp.dream.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * device号码对应到enterprise的表
 * @author 梦某人
 * @date 2021/12/8 15:45
 */
@Entity
@Table(name = "device_enterprise")
public class DeviceToEnterprise {

    @Id
    @Column(name = "iccid",length = 20)
    private String iccid;

    @Column(name = "enterprise_id",length = 10)
    private String enterpriseId;

    @Column(name = "device_Id",length = 10)
    private String deviceId;

    @Column(name = "device_name", length =  20)
    private String deviceName;

    @Column(name = "device_charge", length =  20)
    private String deviceCharge;

    private Long deviceStartTime;

    private Long lastUpdate;

    private Integer deviceStatus;

    private String bigClassName;

    private String smallClassName;

    private String systemVersion;

    public String getBigClassName() {
        return bigClassName;
    }

    public void setBigClassName(String bigClassName) {
        this.bigClassName = bigClassName;
    }

    public String getSmallClassName() {
        return smallClassName;
    }

    public void setSmallClassName(String smallClassName) {
        this.smallClassName = smallClassName;
    }

    public String getDeviceCharge() {
        return deviceCharge;
    }

    public void setDeviceCharge(String deviceCharge) {
        this.deviceCharge = deviceCharge;
    }

    public Long getDeviceStartTime() {
        return deviceStartTime;
    }

    public void setDeviceStartTime(Long deviceStartTime) {
        this.deviceStartTime = deviceStartTime;
    }

    public Long getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(Integer deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    public String getSystemVersion() {
        return systemVersion;
    }

    public void setSystemVersion(String systemVersion) {
        this.systemVersion = systemVersion;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
