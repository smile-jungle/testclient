package me.nlp.dream.entity;

/**
 * Created by yang on 2022/2/10 15:16
 */

import javax.persistence.*;

/**
 * @author 梦某人
 * @date 2021/10/7 9:24
 */
@Entity
@Table(name = "message")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String deviceId;
    @Column
    private String factoryId;
    @Column
    private Long time;
    @Column
    private Integer status;
    /***
     * 0:注册包
     * 1:心跳包
     * 2:设备发出的信息
     * 3:ws向设备下发的指令
     */
    @Column
    private Integer infoType;
    @Column
    private String data;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getFactoryId() {
        return factoryId;
    }

    public void setFactoryId(String factoryId) {
        this.factoryId = factoryId;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getInfoType() {
        return infoType;
    }

    public void setInfoType(Integer infoType) {
        this.infoType = infoType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
