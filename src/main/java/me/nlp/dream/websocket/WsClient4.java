package me.nlp.dream.websocket;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

/**
 * @author 梦某人
 * @date 2021/9/15 9:22
 */
public class WsClient4 extends WebSocketClient {
    public WsClient4(URI serverUri, Draft draft) {
        super(serverUri, draft);
    }

    public WsClient4(URI serverURI) {
        super(serverURI);
    }

    public WsClient4(URI serverUri, Map<String, String> httpHeaders) {
        super(serverUri, httpHeaders);
    }

    @Override
    public void onOpen(ServerHandshake handshakedata) {
    //    send("Hello, it is me. Mario :)");
        System.out.println("opened connection");
        // if you plan to refuse connection based on ip or httpfields overload: onWebsocketHandshakeReceivedAsClient
    }

    @Override
    public void onMessage(String message) {
        JSONObject j = JSONObject.parseObject(message);
        if(j.getInteger("info_type") != 4 && j.getInteger("info_type") != 2 ){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("status",200);
            JSONArray jsonArray  = new JSONArray();
            jsonArray.add("123467832123412412412");
            jsonObject.put("device_id",jsonArray);
            JSONArray jsonArray1 = new JSONArray();
            jsonArray1.add("test");
            jsonObject.put("info_type",1);
            jsonObject.put("data",jsonArray1);
            jsonObject.put("factory_id","004");
            send(jsonObject.toJSONString());
        }
        System.out.println("received: " + message);
    }

    @Override
    public void onClose(int code, String reason, boolean remote) {
        // The codecodes are documented in class org.java_websocket.framing.CloseFrame
        System.out.println(
                "Connection closed by " + (remote ? "remote peer" : "us") + " Code: " + code + " Reason: "
                        + reason);
    }

    @Override
    public void onError(Exception ex) {
        ex.printStackTrace();
        // if the error is fatal then onClose will be called additionally
    }




    public static void main(String[] args) throws URISyntaxException {
        WsClient4 c = new WsClient4(new URI("ws://127.0.0.1:8089/004"));
        c.connect();
        System.out.println("end");
    }
}
