package me.nlp.dream.event;

import com.IceCreamQAQ.Yu.annotation.Event;
import com.IceCreamQAQ.Yu.annotation.EventListener;
import com.IceCreamQAQ.Yu.event.events.AppStartEvent;
import lombok.extern.slf4j.Slf4j;
import me.nlp.dream.constant.Constants;
import me.nlp.dream.entity.DeviceToEnterprise;
import me.nlp.dream.service.AllSqlService;
import me.nlp.dream.service.DeviceToEnterpriseService;
import me.nlp.dream.testClient.TestClient2;

import javax.inject.Inject;

/**
 * Created by yang on 2022/2/13 13:59
 */

@Slf4j
@EventListener
public class StartEvent {

    @Inject
    public  DeviceToEnterpriseService deviceToEnterpriseService;
    @Inject
    public AllSqlService allSqlService;

    @Event
    public void startEvent(AppStartEvent event) throws Exception {

        TestClient2 testClient2 = new TestClient2(Constants.ICCID);


//        TestClient2 testClient2 = new TestClient2("12345678901234567801");
//        TestClient2 testClient2 = new TestClient2("20220411092022041109");

        testClient2.setDeviceToEnterpriseService(deviceToEnterpriseService);
        testClient2.setAllSqlService(allSqlService);

//        TestClient2 testClient3 = new TestClient2("12345678901234567802");
//        testClient3.setDeviceToEnterpriseService(deviceToEnterpriseService);
//        testClient3.setAllSqlService(allSqlService);
    }
}
