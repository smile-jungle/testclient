package me.nlp.dream.service;

import com.IceCreamQAQ.Yu.annotation.AutoBind;
import com.icecreamqaq.yudb.jpa.annotation.Transactional;
import me.nlp.dream.dao.DeviceToEnterpriseDao;
import me.nlp.dream.entity.DeviceToEnterprise;
import me.nlp.dream.pojo.Attribute;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by yang on 2022/2/9 23:50
 */
@AutoBind
public class DeviceToEnterpriseService {

    @Inject
    private DeviceToEnterpriseDao enterpriseDao;

    @Transactional
    public void storeAll(List<DeviceToEnterprise> list){
        for(DeviceToEnterprise deviceToEnterprise : list){
            enterpriseDao.saveOrUpdate(deviceToEnterprise);

        }
    }

    @Transactional
    public DeviceToEnterprise findByIccid(String iccid){
//        System.out.println("DeviceDao:"+enterpriseDao.toString());
        DeviceToEnterprise deviceToEnterprise = enterpriseDao.findByIccId(iccid);
        return deviceToEnterprise;
    }


}
