package me.nlp.dream.service;

import com.IceCreamQAQ.Yu.annotation.AutoBind;
import com.icecreamqaq.yudb.jpa.annotation.Transactional;
import com.icecreamqaq.yudb.jpa.hibernate.HibernateContext;
import org.hibernate.Session;
import org.hibernate.query.NativeQuery;

import javax.inject.Inject;
import java.util.List;

/**
 * 这里执行所有的自定义数据库操作
 * @author 梦某人
 * @date 2021/12/8 15:24
 */
@AutoBind
public class AllSqlService {

    @Inject
    private HibernateContext hibernateContext;

    /***
     * 公司设备表: enterpriseId_device
     * 产品大类表: enterpriseId_first_category
     * 产品小类表: enterpriseId_second_category
     * 设备属性表: enterpriseId_deviceId[0:4]_attribute
     * 设备信息表: enterpriseId_deviceId[0:4]_info
     */
    @Transactional
    public List<Object[]> getSelect(String tableName) {
        try {
            Session session = hibernateContext.getSession("default");
            NativeQuery query = session.createNativeQuery("select * from "+ tableName);
            List<Object[]> o = query.list();
            session.clear();
            return  o;
        }catch (Exception e){
            System.out.println(e);
        }
        return null;
    }

}
