package me.nlp.dream.pojo;

/**
 * @author 梦某人
 * @date 2021/12/8 16:31
 */
public class Attribute {
    String attributeName;
    String attributeType;
    String attributeLength;
    String attributeDigits;
    String attributeRemark;
    String attributeCode;

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeType() {
        return attributeType;
    }

    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }

    public String getAttributeLength() {
        return attributeLength;
    }

    public void setAttributeLength(String attributeLength) {
        this.attributeLength = attributeLength;
    }

    public String getAttributeDigits() {
        return attributeDigits;
    }

    public void setAttributeDigits(String attributeDigits) {
        this.attributeDigits = attributeDigits;
    }

    public String getAttributeRemark() {
        return attributeRemark;
    }

    public void setAttributeRemark(String attributeRemark) {
        this.attributeRemark = attributeRemark;
    }

    public String getAttributeCode() {
        return attributeCode;
    }

    public void setAttributeCode(String attributeCode) {
        this.attributeCode = attributeCode;
    }

}
